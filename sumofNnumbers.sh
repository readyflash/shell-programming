echo "Enter the N natural number to be added "
read N
echo "Enter the $N numbers to be added" 
sum=0
for((i=1;i<=N;i++))
do 
 read num 
 sum=$(bc <<< " $sum + $num ")
done 
 echo $sum
